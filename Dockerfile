FROM ubuntu

RUN apt-get update && \
    apt-get -qq -y install gcc g++ && \
    apt-get -qq -y install wget && \
    apt-get -qq -y install libsfml-dev && \
    apt-get -qq -y install libglu1-mesa-dev freeglut3-dev mesa-common-dev && \
    apt-get -qq -y install xauth && \
    apt-get -qq -y install mesa-utils && \
    apt-get -qq -y install -y xserver-xorg-video-all && \
    apt-get -qq -y install cmake

RUN mkdir Catan
WORKDIR Catan

COPY ./code ./code
COPY ./resources ./resources
COPY ./CMakeLists.txt .

# Building
RUN echo "Building project" && \
    cmake CMakeLists.txt && \
    make

CMD ./catan_game

