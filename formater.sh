#!/bin/bash

find -name *.hpp -o -name *.cpp -o -name *.inl | xargs clang-format -i
